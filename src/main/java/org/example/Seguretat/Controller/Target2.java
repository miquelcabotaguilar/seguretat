package org.example.Seguretat.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/target2")
public class Target2 extends HttpServlet {
    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Dins options de target2");
        addCorsHeader(resp);

    }

    private void addCorsHeader(HttpServletResponse resp){
        resp.addHeader("Access-Control-Allow-Origin", "http://localhost:8090");
        resp.addHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
        resp.addHeader("Access-Control-Allow-Credentials", "true");
        resp.addHeader("Access-Control-Allow-Headers", "Content-type");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Dins Post");
        PrintWriter pw = resp.getWriter();
        pw.println("OK");
        addCorsHeader(resp);
    }
}
