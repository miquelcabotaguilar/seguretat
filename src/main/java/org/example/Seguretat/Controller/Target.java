package org.example.Seguretat.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/target")
public class Target extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("Dins Get de Target 1");
        int n1 = Integer.parseInt(req.getParameter("n1"));
        int n2 = Integer.parseInt(req.getParameter("n2"));
        PrintWriter pw = resp.getWriter();
        pw.printf("Suma: %d", n1+n2);
        System.out.println("Dins Get de Target 2");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        int n1 = Integer.parseInt(req.getParameter("na"));
        int n2 = Integer.parseInt(req.getParameter("nb"));
        PrintWriter pw = resp.getWriter();
        pw.printf("Suma: %d", n1+n2);
        System.out.println("Dins POST Target");
    }
}
